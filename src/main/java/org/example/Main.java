package org.example;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Main {
    public static void main(String[] args) {

        WebDriver driver = new ChromeDriver();

        driver.get("https://pastebin.com/");

        WebElement pasteCodeField = driver.findElement(By.id("postform-text"));
        pasteCodeField.sendKeys("Hello from WebDriver");

        WebElement expirationDropdown = driver.findElement(By.id("select2-postform-expiration-container"));
        expirationDropdown.click();
        WebElement tenMinutesOption = driver.findElement(By.xpath("//li[text()='10 Minutes']"));
        tenMinutesOption.click();

        WebElement pasteNameField = driver.findElement(By.id("postform-name"));
        pasteNameField.sendKeys("helloweb");

        WebElement createButton = driver.findElement(By.xpath("//button[@class='btn -big']"));
        createButton.click();

        String pasteUrl = driver.getCurrentUrl();
        System.out.println("URL of the created paste: " + pasteUrl);

        driver.quit();
    }
}
